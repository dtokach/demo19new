<?php
function get_plans()
{
    $mysqli = new mysqli("localhost", "root", "", "hyip");

    $sql = "SELECT * FROM `plans`";
    $res = $mysqli->query($sql);
    $plans = array();
    while ($row = $res->fetch_assoc()) {
        $plans[] = $row;
    }
    return $plans;
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=1200">
	<title>Главная</title>
	<link href="css/style.css" rel="stylesheet">
	<script src="//cdn.blitz-market.ru/sprite/latest/"></script>
	<script src="js/jquery.min.js"></script>
    <link rel="shortcut icon" href="images/favicon.ico">

	<script>
      $(document).ready(function () {
          $("#js-amount").on("change keyup", function () {

              calc($(this).val());
          });
          $("#js-plan").on("change", function () {
              calc($("#js-amount").val());
          });
          $(document).ready(function () {
              calc($("#js-amount").val());
          });

          function calc(amount) {
              var plans = <?php echo json_encode(get_plans(), true); ?>,
                  plan_id = Number($("#js-plan").val()),
                  percent = 0,
                  count = 0,
                  seconds = 0,
                  plan_return = 0,
                  check = false;
              if (plans) {
                  plans.forEach(function (plan) {
                      if (amount >= Number(plan["min"]) && amount <= Number(plan["max"]) && plan_id === Number(plan["id"])) {
                          count = Number(plan["count"]);
                          percent = Number(plan["percent"]);
                          seconds = Number(plan["seconds"]);
                          plan_return = Number(plan["return"]);
                          check = true;
                      }
                  });
              }
              if (check) {
                  var total = (amount / 100 * percent) * (count === 0 ? 1 : count) + Number(plan_return === 1 ? amount : 0);
                  $("#total").html(total.toFixed(2) + " " + 'usd');
              } else {
                  $("#total").html("-----");
              }
          }
      });
	</script>
</head>

<body>
<div class="wrapper">
	<div class="content">
		<div class="header_upper">
			<div class="container">
				<div class="row">
					<div class="col-xs-3">
						<a class="link" href="login.html">Вход</a>
						<a class="link" href="signup.html">Регистрация</a>
					</div>

					<div class="col-xs-4 col-xs-offset-5">
						<div class="header_social">
							<a href="#">
								<svg role="img">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_social_youtube"></use>
								</svg>
							</a>
							<a href="#">
								<svg role="img">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_social_vk"></use>
								</svg>
							</a>
							<a href="#">
								<svg role="img">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_social_twitter"></use>
								</svg>
							</a>
							<a href="#">
								<svg role="img">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_social_telegram"></use>
								</svg>
							</a>
							<a href="#">
								<svg role="img">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_social_skype"></use>
								</svg>
							</a>
							<a href="#">
								<svg role="img">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_social_instagram"></use>
								</svg>
							</a>
							<a href="#">
								<svg role="img">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_social_facebook"></use>
								</svg>
							</a>
							<a href="#">
								<svg role="img">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_social_mmgp"></use>
								</svg>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<header>
			<div class="container">
				<div class="row">
					<div class="col-xs-3">
						<a href="/" class="logo">
							<img src="images/logo.png" alt="">
						</a>
					</div>

					<div class="col-xs-9">
						<nav>
							<a href="about.html">О клубе</a>
							<a href="faq.html">Faq</a>
							<a href="rules.html">Правила</a>
							<a href="investors.html">Маркетинг</a>
							<a href="reviews.html">Отзывы</a>
							<a href="news.html">Новости</a>
							<a href="support.html">Поддержка</a>
						</nav>
					</div>
				</div>
			</div>
		</header>

		<div class="rates">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<p class="title">Тарифные планы</p>
						<div class="rates_list">
							<div class="rate">
								<div class="top">
									<p class="name">Тариф 1</p>
									<b class="percent">10%</b>
									<i class="for">на 1 день</i>
									<span>Выплаты в конце срока</span>
								</div>

								<div class="bottom">
                                    <span>
                                      min: 1
                                      <svg role="img">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_currency_bold_rub"></use>
                                      </svg>
                                    </span>
                                                    <span>
                                      max: 10000
                                      <svg role="img">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_currency_bold_rub"></use>
                                      </svg>
                                    </span>
								</div>
							</div>

							<div class="rate">
								<div class="top">
									<p class="name">Тариф 1</p>
									<b class="percent">10%</b>
									<i class="for">на 1 день</i>
									<span>Выплаты в конце срока</span>
								</div>

								<div class="bottom">
                                    <span>
                                      min: 1
                                      <svg role="img">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_currency_bold_rub"></use>
                                      </svg>
                                    </span>
                                                    <span>
                                      max: 10000
                                      <svg role="img">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_currency_bold_rub"></use>
                                      </svg>
                                    </span>
								</div>
							</div>

							<div class="rate">
								<div class="top">
									<p class="name">Тариф 1</p>
									<b class="percent">10%</b>
									<i class="for">на 1 день</i>
									<span>Выплаты в конце срока</span>
								</div>

								<div class="bottom">
                                    <span>
                                      min: 1
                                      <svg role="img">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_currency_bold_rub"></use>
                                      </svg>
                                    </span>
                                                    <span>
                                      max: 10000
                                      <svg role="img">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_currency_bold_rub"></use>
                                      </svg>
                                    </span>
								</div>
							</div>
						</div>

						<div class="statistics">
							<p class="sub_title">Статистика</p>
							<table>
								<tr>
									<td>Мы работаем</td>
									<td>302 дня</td>
								</tr>

								<tr>
									<td>Всего участников</td>
									<td>1</td>
								</tr>

								<tr>
									<td>Активных аккаунтов</td>
									<td>1</td>
								</tr>

								<tr>
									<td>Принято средств</td>
									<td>
										0
										<svg role="img">
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_currency_bold_rub"></use>
										</svg>
									</td>
								</tr>

								<tr>
									<td>Выплачено средств</td>
									<td>
										0
										<svg role="img">
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_currency_bold_rub"></use>
										</svg>
									</td>
								</tr>

								<tr>
									<td>Последний вклад</td>
									<td>
										0
										<svg role="img">
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_currency_bold_rub"></use>
										</svg>
									</td>
								</tr>

								<tr>
									<td>Последняя выплата</td>
									<td>
										0
										<svg role="img">
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_currency_bold_rub"></use>
										</svg>
									</td>
								</tr>
							</table>
						</div>

						<section class="calc">
							<p class="sub_title">Калькулятор</p>
							<div class="calc_el">
								<label for="">Сумма (
									<svg role="img">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_currency_bold_rub"></use>
									</svg>
									):</label>
								<input name="sum" id="js-amount" type="text" value="50.00" class="calc_input">
							</div>
							<div class="calc_el">
								<label for="">Выбор плана:</label>
								<select name="plan" id="js-plan" class="calc_select">
                                    <?php foreach (get_plans() as $item) { ?>
                                        <option value="<?php echo $item['id']; ?>">
                                            <?php echo $item['description']; ?>
                                        </option>
                                    <?php } ?>
								</select>
							</div>
							<div class="calc_result">
								<label for="">Вы получаете:</label>
								<div class="calc_result_amount">
									<span id="total"></span>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>

        <div class="advantages">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="title">Наши преимущества</p>
                        <div class="adv">
                            <div class="adv_item">
                                <svg role="img">
                                    <use xlink:href="images/sprite.svg#script"></use>
                                </svg>
                                <p>Лицензионный скрипт</p>
                            </div>

                            <div class="adv_item">
                                <svg role="img">
                                    <use xlink:href="images/sprite.svg#servers"></use>
                                </svg>
                                <p>Выделенный сервер</p>
                            </div>

                            <div class="adv_item">
                                <svg role="img">
                                    <use xlink:href="images/sprite.svg#ddos"></use>
                                </svg>
                                <p>DDoS защита</p>
                            </div>

                            <div class="adv_item">
                                <svg role="img">
                                    <use xlink:href="images/sprite.svg#ssl"></use>
                                </svg>
                                <p>Ssl сертификат</p>
                            </div>

                            <div class="adv_item">
                                <svg role="img">
                                    <use xlink:href="images/sprite.svg#plans"></use>
                                </svg>
                                <p>Надежные планы</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="double_theme__block">
            <div class="container">
                <div class="row">
                    <div class="col-xs-8">
                        <div class="about_index">
                            <p class="sub_title">О клубе</p>
                            <div class="text">
                                <p>Инвестиционные проекты чаще всего прикрыты красивой легендой, но проценты выплачивают с позже поступивших вкладов. Когда приток денег становится меньше, чем нужно выплачивать, проект закрывают.</p>
                                <p>Мы не придумываем легенд. Мы просто прокручиваем поступившие инвестиции в матричном партнерском проекте. Никто не сомневается в надежности матриц и не боится, что матричный проект закроется, потому что матрицы живут, пока есть денежный оборот. Но даже при большом потенциале получения быстрой прибыли все знают, что приглашать в матрицы очень тяжело.</p>
                                <p><a href="about.html">Подробнее...</a></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="news_index">
                            <p class="sub_title">Наши новости</p>
                            <div class="news_preview">
                                <a href="news.html" class="name">Первая новость</a>
                                <span>10.01.2018 19:05:44</span>
                                <a href="news.html" class="main_link">Все новости</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>

	<footer>
        <div class="tops_line">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <a href="topin.html" class="main_link">Топ инвесторов</a>
                        <a href="lastin.html" class="main_link">Последние депозиты</a>
                        <a href="lastout.html" class="main_link">Последние выплаты</a>
                        <a href="top.html" class="main_link">Топ рефералов</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <nav>
                        <a href="about.html">О клубе</a>
                        <a href="faq.html">Faq</a>
                        <a href="rules.html">Правила</a>
                        <a href="investors.html">Маркетинг</a>
                        <a href="reviews.html">Отзывы</a>
                        <a href="news.html">Новости</a>
                        <a href="support.html">Поддержка</a>
                    </nav>
                </div>
            </div>
        </div>

        <div class="downer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-9">
                        <div class="ps">
                            <a href="">
                                <svg role="img">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_mini_logos_transparent_yandexmoney"></use>
                                </svg>
                            </a>

                            <a href="">
                                <svg role="img">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_mini_logos_transparent_qiwi"></use>
                                </svg>
                            </a>

                            <a href="">
                                <svg role="img">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_mini_logos_transparent_nixmoney"></use>
                                </svg>
                            </a>

                            <a href="">
                                <svg role="img">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_mini_logos_transparent_perfectmoney"></use>
                                </svg>
                            </a>

                            <a href="">
                                <svg role="img">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_mini_logos_transparent_litecoin"></use>
                                </svg>
                            </a>

                            <a href="">
                                <svg role="img">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_mini_logos_transparent_payeer"></use>
                                </svg>
                            </a>

                            <a href="">
                                <svg role="img">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_mini_logos_transparent_okpay"></use>
                                </svg>
                            </a>

                            <a href="">
                                <svg role="img">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_mini_logos_transparent_ethereum"></use>
                                </svg>
                            </a>

                            <a href="">
                                <svg role="img">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_mini_logos_transparent_dogecoin"></use>
                                </svg>
                            </a>

                            <a href="">
                                <svg role="img">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_mini_logos_transparent_advcash"></use>
                                </svg>
                            </a>

                            <a href="">
                                <svg role="img">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#bm_icon_mini_logos_transparent_bitcoin"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-xs-3">
                        <a href="/" class="logo">
                            <img src="images/logo.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
	</footer>
</div>


<script src="js/bootstrap.js"></script>

</body>
</html>